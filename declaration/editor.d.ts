/**
 * 编辑器命名空间
 */
declare namespace Editor {

    /**
     * AssetDB 实例
     */
    export class assetdb {

        /**
         * Get native file path by url.
         */
        static queryPathByUrl(url: string, callback: (err: any, path: any) => void);

        /**
         * Get uuid by url.
         */
        static queryUuidByUrl(url: string, callback: (err: any, uuid: any) => void);

        /**
         * Get native file path by uuid.
         */
        static queryPathByUuid(uuid: string, callback: (err: any, path: any) => void);

        /**
         * Get asset url by uuid.
         */
        static queryUrlByUuid(uuid: string, callback: (err: any, url: any) => void);

        /**
         * Get asset info by uuid.
         */
        static queryInfoByUuid(uuid: string, callback: (err: any, info: any) => void);

        /**
         * Get meta info by uuid.
         */
        static queryMetaInfoByUuid(uuid: string, callback: (err: any, info: any) => void);

        /**
         * Query all assets from asset-db.
         */
        static deepQuery(callback: (err: any, results: any[]) => void);

        /**
         * Query assets by url pattern and asset-type.
         */
        static queryAssets(pattern: string, assetTypes: string | string[], callback: (err: any, results: any[]) => void);

        /**
         * Import files outside asset-db to specific url folder. 
         */
        static import(rawfiles: string[], destUrl: string, showProgress?: boolean, callback?: (err: any, result: any) => void);

        /**
         * Create asset in specific url by sending string data to it.
         */
        static create(url: string, data: string, callback?: (err: any, result: any) => void);

        /**
         * Move asset from src to dest.
         */
        static move(srcUrl: string, destUrl: string, showMessageBox?: boolean);

        /**
         * Delete assets by url list.
         */
        static delete(urls: string[]);

        /**
         * Save specific asset by sending string data.
         */
        static saveExists(url: string, data: string, callback?: (err: any, result: any) => void);

        /**
         * Create or save assets by sending string data. If the url is already existed, it will be changed with new data. The behavior is same with method saveExists. Otherwise, a new asset will be created. The behavior is same with method create.
         */
        static createOrSave(url: string, data: string, callback?: (err: any, result: any) => void);

        /**
         * Save specific meta by sending meta's json string.
         */
        static saveMeta(uuid: string, metaJson: string, callback?: (err: any, result: any) => void);

        /**
         * Refresh the assets in url, and return the results.
         */
        static refresh(url: string, callback?: (err: any, results: any[]) => void);

    }

}